##============================================================================##
##                     MATCHING GENE-DRUG INTERACTIONS                        ##
##----------------------------------------------------------------------------##
##                                                                            ##
## Set of functions that search for matching variants (SNVs, CNVs and fusion  ##
## genes) in specialized  gene-drug interaction databases:                    ##
##                                                                            ##
##============================================================================##

setwd(paste0(INSTALL_DIR,"/data/formated"))

#############
## 1) GDKD ##
#############
match_SNV_GDKD = function(snv,db = read.delim("inputGDKD.csv",sep="\t",stringsAsFactors = FALSE)){
  ## Given a list of SNVs, searches for matching variants in Gene Drug Knowledge Database (Dienstmann et al., 2015).
  ## input: snv is a dataframe with SNVs. Must have three columns: gene symbol, variant classification and amino_acid_change (AXXXB).
  ## output: returns those Gene Drug Knowledge Database rows matching to the input SNVs.
  
  gdkd=db
  #gdkd = read.delim("data/GDKD.csv",sep="\t")
  gdkd$Patient_variant = ""
  gdkd$Supporting_Database = "GDKD"
  gdkd$nct_ids = NA
  druggable                 = data.frame(matrix(ncol=ncol(gdkd),nrow=0))
  colnames(druggable)       = colnames(gdkd)
  colnames(druggable)       = colnames(gdkd)
  
  ## We apply a narrowing proceedure
  ## Starting with all variants of the database
  ## We filter out genes in the database not mutated in the patient
  ## Replace 'sensitivity' with 'response'
  gdkd$Gene= gsub(" ", "",gdkd$Gene)
  index               = as.character(gdkd$Gene) %in% as.character(snv[,1])  #Database genes mutated in patient
  druggable           = subset(gdkd,index)                                  #Subset from complete DB
  rownames(druggable) = NULL
  gdkd$Association_1  = gsub("sensitivity", "response", gdkd$Association_1)

  ## From the subset previously filtered by gene
  ## We annotate only matching variants or repurposed ones (NEW)
  if (nrow(druggable) != 0){
  
    for (i in 1:nrow(druggable)){
    
      gene       =  druggable[i,"Gene"]
      p_var_type = as.character(snv[which(as.character(snv[,1])==gene),2]) # Patient's variant type on current gene
      p_variants = unique(as.character(snv[which(as.character(snv[,1])==gene),3])) # Patient's protein change on the current gene
      for (p_variant in p_variants){
      
        ### Match Missense mutations with variant not specified in the database
        if (grepl("missense|mutation|any",druggable[i,"Description"], ignore.case = TRUE) && 
          grepl("mut|any|unknown",druggable[i,"Variant"], ignore.case = TRUE) && 
          grepl("missense|frameshift",p_var_type,ignore.case = TRUE) ){
          druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant)
        }
        
        ### Match Missense mutations with specific variant specified in the database
        if (grepl("missense",druggable[i,"Description"], ignore.case = TRUE) && grepl("missense",p_var_type,ignore.case = TRUE) && grepl("mut|any|unknown",druggable[i,"Variant"],ignore.case = TRUE)==FALSE){
          if (grepl(p_variant,druggable[i,"Variant"])){ 
            druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant) }
          if (str_sub(p_variant, start = 1, end = -2) %in% unlist(strsplit(as.character(druggable[i,"Variant"]), ", "))) {
            druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant) }

          ## Repurposing - New variants         
#          else { 
#            if (grepl(gene,"BRAF,KIT,FGFR3,KRAS,PDGFRA,EGFR,AKT1,MTOR,ALK",ignore.case = TRUE)){ 
#              if (gene=="EGFR" &&  druggable[i,"Variant"] != "T790M" && druggable[i,"Variant"] != "C797S"){
#                druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],paste("NEW",p_variant,sep=" "),sep=" ")}
#              if (gene=="MTOR" && druggable[i,"Variant"] != "F2108L"){
#                druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],paste("NEW",p_variant,sep=" "),sep=" ")}
#              if (gene=="AKT1" && druggable[i,"Variant"] != "Q79K"){
#                druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],paste("NEW",p_variant,sep=" "),sep=" ")}
#              if (gene=="BRAF" && grepl("V600",druggable[i,"Variant"])==F){
#                druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],paste("NEW",p_variant,sep=" "),sep=" ")}
#            }
#          }
        }
        
        ### Match indels, small deletions and insertions - repurposing rule (position is not checked)
        if (grepl("indel|deletion mutation|insertion mutation",druggable[i,"Description"], ignore.case = TRUE) && 
          grepl("ins|del",p_var_type,ignore.case = TRUE) ){
          druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant)
        }
        
        ### Match Nonsense mut - repurposing rule (deletion, LoF)
        if (grepl("loss-of-function",druggable[i,"Effect"], ignore.case = TRUE) && 
          grepl("nonsense",p_var_type,ignore.case = TRUE) ){
          druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],paste("NEW",p_variant,sep=""),sep=" ")
        }
        
        ### Find matches in variants annotated at exon level (exon X p.XX-XX)
        if (grepl("p\\.[0-9]+-[0-9]+",druggable[i,"Variant"])){
          aa_pos = as.numeric(str_match(p_variant,"[0-9]+"))
          range  = unique(as.character(str_match_all(as.character(druggable[i,"Variant"]),"p\\.[0-9]+-[0-9]+")[[1]]))
          range  = strsplit(gsub("p\\.", "", range),"-")
          if (length(range) != 0){
            for (x in 1:length(range)){
              if (aa_pos >= range[[x]][1] && aa_pos <= range[[x]][2]){
                druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant)
                druggable[i,"Variant"] = paste(druggable[i, "Variant"], "(reg)")
              } 
            }
          }
        }
      }
    }
  }

  ## Keep all those entries with "Patient_variant"
  index= which(druggable$Patient_variant != "")
  druggable=druggable[index,]   #Subset from first selection
                                       
  return(druggable)
  
}

match_WT_GDKD = function(snv, cancer_gdkd,db = read.delim("inputGDKD.csv",sep="\t")){
  ## Searches for wild type variants in Gene Drug Knowledge Database not matching any gene in the inputs SNVs or CNVs.
  ## input: snv is a dataframe with SNVs. Must have three columns: gene symbol, variant classification and amino_acid_change (AXXXB).
  ## input: cnv is a dataframe with CNVs. Must have two columns: gene symbol, variant (amplification or deletion).
  ## input: cancer_gdkd is a character vector with a cancer type used by GDKD.
  ## output: returns those GDKD wild type rows (annotated for disease=cancer_gdkd) not matching to the input SNVs and CNVs.

  gdkd = db
  gdkd$Patient_variant = ""
  druggable = data.frame(matrix(ncol=ncol(gdkd),nrow=0))
  colnames(druggable)       = colnames(gdkd)
   
  wt  = grep("wild type",gdkd[,"Variant"],ignore.case=TRUE)
  wt  = wt[!(gdkd[wt,"Gene"] %in% c(as.character(snv[,1])))] # keep those that are not in snv


  #check cancer type=cancer_gdkd
  same_cancer=c()
  for (ck in cancer_gdkd){
    same_cancer = c(which(gdkd[wt,"Disease"]==ck),same_cancer)
  }
  druggable  = gdkd[wt[unique(same_cancer)],]
  
  return(druggable)
  
}


################
## 2) CIVIC   ##
################
match_SNV_CIVIC = function(snv,db = read.delim("inputCIVIC.csv",sep="\t")){
  ## Given a list of SNVs searches for matching variants in CIViC database.
  ## input: snv is a dataframe with SNVs. Must have three columns: gene symbol, variant classification and amino_acid_change (AXXXB).
  ## output: returns those CIViC rows matching to the input SNVs.

  civic = db
  civic$Patient_variant = ""
  civic$Supporting_Database = "CIViC"
  druggable             = data.frame(matrix(ncol=ncol(civic),nrow=0))
  colnames(druggable)   = colnames(civic)
  
  # Gene match
  index               = as.character(civic$gene) %in% as.character(snv[,1])  #Database genes mutated in patient
  druggable           = subset(civic,index)                                  #Subset from complete DB
  rownames(druggable) = NULL

 # Keep matching variants
  if (nrow(druggable) != 0){
    for (i in 1:nrow(druggable)){
      gene       = druggable[i,"gene"]
      p_var_type = as.character(snv[which(as.character(snv[,1])==gene),2])
      p_variants = unique(as.character(snv[which(as.character(snv[,1])==gene),3]))
      for (p_variant in p_variants){
        ### Any mutation
        if (druggable[i,"variant"]=="MUTATION" || druggable[i,"variant"]=="LOSS-OF-FUNCTION"){
          druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant)
        }
        ### frameshift,ins,del
        if (grepl("frameshift|frame shift",druggable[i,"variant"], ignore.case = TRUE) && grepl("frameshift",p_var_type,ignore.case = TRUE) ){
          druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant)
        }
        ### stopgain ~ deletion
        if (grepl("deletion|loss",druggable[i,"variant"], ignore.case = TRUE) && grepl("nonsense",p_var_type,ignore.case = TRUE) ){
          druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],paste("NEW",p_variant,sep=" "))
        }
        ### Missense & Specific variant
        if (grepl("missense",p_var_type[match(p_variant,p_variants)],ignore.case = TRUE)){
          if (grepl(p_variant,druggable[i,"variant"])){ 
            druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant)
          }
        }
      }
    }
  }

  ## Keep all those entries with "Patient_variant" and WT
  index      = which(druggable$Patient_variant != "")
  druggable  = druggable[index,]   #Subset from first selection
 # druggable  = rbind(druggable,civic[wt,])
                                
  return(druggable)
  
}

match_WT_CIVIC  = function(snv, cancer_civic,db = read.delim("inputCIVIC.csv",sep="\t")){
  ## Searches for wild type variants in CIViC not matching any gene in the inputs SNVs or CNVs.
  ## input: snv is a dataframe with SNVs. Must have three columns: gene symbol, variant classification and amino_acid_change (AXXXB).
  ## input: cnv is a dataframe with CNVs. Must have two columns: gene symbol, variant (amplification or deletion).
  ## input: cancer_civic is a character vector with a cancer type used by CIViC.
  ## output: returns those CIViC wild type rows (annotated for disease=cancer_civic) not matching to the input SNVs and CNVs.

  civic = db
  wt  = grep("wild type",civic[,"variant"],ignore.case=TRUE)
  wt  = wt[!(civic[wt,"gene"] %in% c(as.character(snv[,1])))] # keep those that are not in snv

  #check cancer type=cancer_civic
  same_cancer=c()
  for (ck in cancer_civic){
    same_cancer = c(which(civic[wt,"disease"]==ck),same_cancer)
  }

  druggableCIVIC_wt  = civic[same_cancer,]
  
  return(druggableCIVIC_wt)
}


################
## 3) CGI     ##
################
match_SNV_CGI = function(snv,db = read_delim("inputCGI.tsv","\t", escape_double = FALSE, trim_ws = TRUE)){
cgi = as.data.frame(db)
cgi$Patient_variant = ""
cgi$Supporting_Database = "CGI"
druggable             = data.frame(matrix(ncol=ncol(cgi),nrow=0))
colnames(druggable)   = colnames(cgi)

# Gene match
index               = as.character(cgi$Gene) %in% as.character(snv[,1])  #Database genes mutated in patient
druggable           = subset(cgi,index)                                  #Subset from complete DB
rownames(druggable) = NULL

# Keep matching variants
if (nrow(druggable) != 0){
  for (i in 1:nrow(druggable)){
    gene       = druggable[i,"Gene"]
    p_var_type = as.character(snv[which(as.character(snv[,1])==gene),2])
    p_variants = unique(as.character(snv[which(as.character(snv[,1])==gene),3]))
    for (p_variant in p_variants){
      ### Any mutation
      if (druggable[i,"Alteration type"] == "MUT"){
        if (grepl("oncogenic mutation", druggable[i,"Biomarker"])){ 
          druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant)
        }    
        if (grepl("missense",p_var_type[match(p_variant,p_variants)],ignore.case = TRUE)){
          if (grepl(p_variant,druggable[i,"Biomarker"])){ 
            druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant)
          }
          if (str_sub(p_variant, start = 1, end = -2) %in% unlist(strsplit(druggable[i,"Biomarker"], ","))) {
            druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant) }
        }
        if (grepl("[0-9]+-[0-9]+",druggable[i,"Alteration"])) {
          aa_pos = as.numeric(str_match(p_variant,"[0-9]+"))
          range  = unique(as.character(str_match_all(as.character(druggable[i,"Alteration"]),"[0-9]+-[0-9]+")[[1]]))
          range  = strsplit(range,"-")
          if (length(range) != 0){
            for (x in 1:length(range)){
              if (aa_pos >= range[[x]][1] && aa_pos <= range[[x]][2]){
                druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant)
                druggable[i, "Biomarker"] = paste(druggable[i, "Biomarker"], "(reg)")
              }
            }
          }
        }
      }
    }  
  }
}

#if (str_sub(p_variants, start = 2, end = -2) %in% as.numeric(unlist(regmatches(gsub(".*:","",druggable[i,"Alteration"]), gregexpr("[[:digit:]]+",gsub(".*:","",druggable[i,"Alteration"])))))) {
#  druggable[i,"Patient_variant"] = paste(druggable[i,"Patient_variant"],p_variant)
#}


## Keep all those entries with "Patient_variant" and WT
index      = which(druggable$Patient_variant != "")
druggable  = druggable[index,]   #Subset from first selection
return(druggable)
}
